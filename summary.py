#!/usr/bin/env python
# coding: utf-8
# User Question Records

# - user_id, user_name
# - date
# - tag
# - full question (text)
# - chat_id

# output (daily):

# - number of inquirers
# - number of messages
# - top asked tags

import numpy as np
# get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib as plt
import pandas as pd
from datetime import datetime
import datetime
import time


# import the user question records 

import json

with open('records/user_texts.json', encoding='utf-8') as json_file:
	user_texts = json.load(json_file) # turn json to list

#print (type(user_texts))
#print (len(user_texts))
#print (user_texts)


# sort today's records

today = datetime.date.today()

today_records = []

for i in range(len(user_texts)):

    d = user_texts[i]["date"]
    time_local = time.localtime(d)
    dt = time.strftime('%Y-%m-%d', time_local)
    print(dt)

    if dt == str(today):
        today_records.append(user_texts[i])

#print (today_records)
#print (len(today_records))


# import the corpus 

import json

with open('corpus/corpus.json', encoding='utf-8') as json_file:
	data = json.load(json_file)
#print (type(data))

# get all tags in the corpus
all_tags = []

for item in data:
    tag = item['tag']
    all_tags.append(tag)

#print (all_tags)


# calculate number of inquirers everyday 

all_daily_users = []

for item in today_records:
    u = item['user_id']
    all_daily_users.append(u)

#print (all_daily_users)

unique_all_daily_users = set(all_daily_users)
#print (unique_all_daily_users)

num_daily_users = len(unique_all_daily_users)
#print (num_daily_users)


# calculate number of messages everyday 

all_daily_msgs = []

for item in today_records:
    msg = item['text']
    all_daily_msgs.append(msg)

# print (all_daily_msgs)

num_daily_msgs = len(all_daily_msgs)
#print (num_daily_msgs)



# top 3 asked tags everyday

daily_asked_tags = []

for i in all_daily_msgs:
    if i in all_tags:
        daily_asked_tags.append(i)

# print (daily_asked_tags)

dict_daily_asked_tags = {}

for key in daily_asked_tags:
    dict_daily_asked_tags[key] = dict_daily_asked_tags.get(key, 0) + 1

# print (dict_daily_asked_tags)

sorted_list = sorted(dict_daily_asked_tags,key=lambda x:dict_daily_asked_tags[x])
# print (sorted_list)

top_asked_tags = sorted_list[-3:][::-1]
#print(top_asked_tags)


b_list = []
for i in range(len(top_asked_tags)):
    a = top_asked_tags[i]
    b = str(i+1) + "." + str(a)
    b_list.append(b)
#print (b_list)

str_top_asked_tags = " ".join(b_list)
#print (str_top_asked_tags)


def generate_summary():
    
    summary = "Hi, Jackie! Today a total of " + str(num_daily_users)  + " people came to ask me questions, and I received " + str(num_daily_msgs) + " messages in total. The most frequently asked questions today was about " + str_top_asked_tags + "."
    
    return summary


# generate_summary()



