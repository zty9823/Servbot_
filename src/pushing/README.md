# README.md for text and survey pushing feature.

## Data structure for storage
* type: string - "text", "survey"
* question: string
* content: string
* time: string
* period: string - Once 0, Daily 1, Weekly 2, Monthly 3
* target: string - "all", "group"
