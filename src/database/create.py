import sqlite3

conn = sqlite3.connect('database.db')
print("Opened database successfully")

# conn.execute('DROP TABLE test_pushing')
# print("Table dropped successfully")


# conn.execute('CREATE TABLE test_pushing (type TEXT, question TEXT, option TEXT, content TEXT, date TEXT, time TEXT, period TEXT, target TEXT)')
# print("Table created successfully")
# conn.close()

# conn.execute('CREATE TABLE message_pushing (type TEXT, question TEXT, content TEXT, date TEXT, time TEXT, period TEXT, target TEXT)')
# print("Table created successfully")
# conn.close()

# with sqlite3.connect("database.db") as con:
#     time = 'qer'
#     period ='tw'
#     target = 'qef'
#     cur = con.cursor()
#     cur.execute("INSERT INTO test_pushing (time, period, target) VALUES (?,?,?)",(time, period, target) )
#     con.commit()
#     print("connect successfully")
#     cur.close()

# scrpit to print db content
con = sqlite3.connect("database.db")
# con.row_factory = sqlite3.Row
# cur = con.cursor()
# cur.execute("select * from test_pushing")
for row in con.execute("select * from test_pushing"):
    print(row)

# rows = cur.fetchall()
# print(rows[0], rows[1])
con.close()