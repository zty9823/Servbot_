
from flask import Flask, request, render_template, redirect, url_for
from flask_cors import CORS
import requests
import json
import csv
import os

from requests.api import delete
from telebot_files.credentials import bot_token, bot_user_name, URL
import telebot
from telegram.ext import Updater, MessageHandler, Filters
import telegram
from summary import generate_summary
from auto_suggestion import match_input_with_tags
# from src.func.convert_excel_json.index import excel2json

import sqlite3 as sql

global bot
global TOKEN
TOKEN = bot_token
bot = telegram.Bot(token=TOKEN)
CORPUS = "corpus/corpus.json"
TABLE_NAME = "test_pushing"

NGROK = "e8aa-108-169-200-180"
# bot = telebot.TeleBot(TOKEN)

msg_dict = {}

QIDtoAnswerDcit = {}
QIDtoQuestionDict = {}
LabeltoQIDDict = {}
TagtoQIDDict = {}
global STATE
STATE = "init"
global CURRENT_QID
def createQuestionDB(corpus):
    with open(corpus, encoding='utf-8') as file:
        load_data = json.load(file)
        for item in load_data:
            if item["label"] not in LabeltoQIDDict.keys():
                LabeltoQIDDict[item["label"]] = [item["qid"]]
            else:
                LabeltoQIDDict[item["label"]].append(item["qid"])
            QIDtoAnswerDcit[item["qid"]] = item["answer"]
            QIDtoQuestionDict[item["qid"]] = item["question"]
            TagtoQIDDict[item["tag"]] = item["qid"]

app = Flask(__name__)

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

def sendmessage(chatid, reply):
    url = "https://api.telegram.org/bot{}/sendMessage".format(TOKEN)
    payload = {
        "text":reply,
        "chat_id":chatid
        }
    resp = requests.get(url,params=payload)
    print("send successful")


@app.route('/test', methods=['POST'])
def test():
    return "test done"

def replyLabelList():
    label_list = LabeltoQIDDict.keys()
    labels = "Here are all labels you can choose: \n"
    for item in label_list:
        labels = labels + '/' + item + '\n'
    labels += "Click a label to see corresponding questions."
    return labels

def replyQuestionList(key):
    key = key[1:]
    if key not in LabeltoQIDDict.keys():
        return "Sorry we do not have this key word. Can you try another one?"
    question = []
    qid = []
    for item in LabeltoQIDDict[key]:
        qid.append(item)
        question.append(QIDtoQuestionDict[item])
    reply = "Do you want to ask the following questions?\n"
    for i in range(len(qid)):
        reply += '/'+ str(qid[i]) + ' - ' + question[i] + "\n"
    reply += "Click a number to get the corresponding answer."
    return reply

def replyAnswer(qid):
    print(qid)
    try:
        reply = QIDtoAnswerDcit[int(qid[1:])]
    except:
        reply = "Sorry, I cannot give you a corresponding reply."
    return reply

def replyQuestionListUsingTag(tag):
    reply = "Here is the suggested question.\n"
    print(tag)
    tag = tag[1:]
    if tag not in TagtoQIDDict.keys():
        return ""
    qid = TagtoQIDDict[tag]
    question = QIDtoQuestionDict[qid]
    reply += '/'+ str(qid) + ' - ' + question + "\n"
    reply += "If you want to ask this question, please choose the index."
    return reply

def replyFreqQuestionList():
    reply = "Here are frequently asked questions.\n"
    for key in QIDtoQuestionDict.keys():
        question = QIDtoQuestionDict[key]
        reply = reply + '/'+ str(key) + ' - ' + question + "\n"
    reply += "If you want to ask one of these questions, please choose the corresponding index."
    return reply

def replyEndWords():
    reply = "Has this answer solved you question?\n"
    reply += "/Yes\n/No"
    return reply

def collectUserFeedback(txt):
    header = ['qid', 'feedback']
    with open('feedback/collection.csv', 'a+') as csvfile:
        # creating a csv writer object 
        csvwriter = csv.writer(csvfile) 
            
        # # writing the header 
        # csvwriter.writerow(header) 
            
        # writing the data rows 
        feedback = 0
        if txt[1:] == "Yes":
            feedback = 1
        qid = CURRENT_QID[1:].strip('\n')
  
        csvwriter.writerow([int(qid), feedback])
    return

# def updateCorpus(path):
#     CORPUS

def downloader(file_id):
    s = requests.get("https://api.telegram.org/bot{}/getFile?file_id={}".format(TOKEN,file_id))
    res = json.loads(s.text)
    file_path = res['result']['file_path']
    r = requests.get("https://api.telegram.org/file/bot{}/{}".format(TOKEN,file_path))
    with open("data/knowledge_base.xlsx", "wb") as f:
        f.write(r.content)
    excel2json("data/knowledge_base.xlsx", "data/knowledge_base.json")
    # updateCorpus("data/knowledge_base.json")
# delete
def auto_suggestion(word):
    return "mock\n/Scholarship \n end"

def send_msg_pushing_link():
    return "You can click the link below and follow the instructions on the website to create a message.\n" + "https://" + NGROK + ".ngrok.io/" + "create"

# mock
def push_message():
    # chatid = "1992437350"
    chatid = "1745681029"
    con = sql.connect("database.db")
    content = ""
    options = ""
    for row in con.execute("select * from " + TABLE_NAME):
        print(row)
        if (row[0] == 'survey'):
            content = row[1]
            if row[2] == "single":
                pass
            elif row[2] == "multi":
                pass
            elif row[2] == "rating":
                options = "Please choose a score from 1 to 5.\n /1 /2 /3 /4 /5."
            else:
                options = "Please type your answer in the text box and send it directly."
        else:
            content = row[3]
    con.close()
    begin = "Hi! Here is a message from CT Student Service for you. Please take time to read and reply your answer. Thank you!\n\n"
    sendmessage(chatid, begin + content + '\n\n' + options)
    return

def collectSurveyAnswer(text):
    pass

def collectSuggestions(text):
    pass

def collectTreeHole(text):
    pass

@app.route("/",methods=["POST","GET"])
def index():
    # fileId = "BQACAgUAAxkBAAIC22FOOmF0_p-jDrezdzlZSozu3zowAAJbBAAC_hpxVjO7BNedBrheIQQ"
    # downloader(fileId)
    # print('end')
    # return
    if(request.method == "POST"):
        resp = request.get_json()
        print(resp)
        if "message" in resp.keys():
            msg = "message"
        elif 'edited_message' in resp.keys():
            msg = "edited_message"
        if "document" in resp[msg].keys():
            fileId = resp[msg]["document"]["file_id"]
            print(fileId)
            # downloader(fileId)
        try:
            msgtext = resp[msg]["text"]
        except:
            msgtext = ""
        sendername = resp[msg]["from"]["first_name"]
        chatid = resp[msg]["chat"]["id"]
        # reply = "You can try to run /label to see suggested questions."
        reply = ""
        global STATE
        global CURRENT_QID
        print("===============================", msgtext, STATE)
        # To reset chatbot if having bugs
        if msgtext == "/back":
            STATE = "init"
            print("====================", STATE)
            return "Hello"
        if STATE == "label":
            reply = replyQuestionList(msgtext)
            STATE = "question"
        elif STATE == "question":
            reply = replyAnswer(msgtext)
            CURRENT_QID = msgtext
            sendmessage(chatid, reply)
            STATE = "collect"
            reply = replyEndWords()
        elif STATE == "tag":
            reply = replyQuestionListUsingTag(msgtext)
            STATE = "question"
        elif STATE == "collect":
            STATE = "init"
            # To Collect Response
            collectUserFeedback(msgtext)
            reply = "Thank you for your respond. :)"
        elif STATE == "send_answer":
            STATE = "init"
            # To Collect Survey Answer
            collectSurveyAnswer(msgtext)
            reply = "Thank you for your respond. :)"
        elif msgtext == '/finish':
            if STATE == "suggestion":
                STATE = "init"
                reply = "Thank you for your suggestion!."
            elif STATE == "tree_hole":
                STATE = "init"
                reply = "Thank you for sharing your stories with me! My suggestion is to try to focus on doing one thing at a time, and finish them one by one. If you need further help, I recommend you have a chat with Cornell Mental Health team. \nHere is the website: https://health.cornell.edu/get-care/appointments. \nHave a nice day!"
                # reply = "Thank you for sharing your stories with me! I recommend you have a chat with Cornell Mental Health team. \n Here is the website: https://health.cornell.edu/get-care/appointments."
        elif STATE == "suggestion":
            collectSuggestions(msgtext)
        elif STATE == "tree_hole":
            collectTreeHole(msgtext)
        elif msgtext == '/label':
            STATE = "label"
            reply = replyLabelList()
        elif msgtext == '/question':
            STATE = "question"
            reply = replyFreqQuestionList()
        elif msgtext == '/summary':
            reply = generate_summary()
        elif msgtext == '/message_pushing':
            reply = send_msg_pushing_link()
        elif msgtext == '/suggestion':
            STATE = "suggestion"
            reply = "Welcome to give any suggestions! If you have said all you want to say, please click or type /finish."
        elif msgtext == '/tree_hole':
            STATE = "tree_hole"
            reply = "Welcome to say anything you want to share with me! If you have said all you want to say, please click or type /finish."
        elif msgtext == '/send_message':
            push_message()
            STATE = "send_answer"
            print('/send_message', STATE)
            reply = ""
        else:
            reply = match_input_with_tags(msgtext)
            if reply == "":
                reply = "Sorry that there is no matched label. You can try to run /label to see suggested questions."
            STATE = "tag"
        print('final:', reply)
        sendmessage(chatid, reply)
    return "Hello"

# Test combination of Flask and Vue
@app.route("/ping", methods=('GET', 'POST'))
def ping():
    def load_mock_data(file):
        with open(file) as json_file:
            data = json.load(json_file)
            return data
        
    response_object = {}
    if request.method == 'POST':
        post_data = request.get_json()
        print(post_data)
        # Fetch data by day
        if post_data['duration'] == 1:
            response_object = load_mock_data('mock_data/key_metrix.json')
            response_object['status'] = 'success'
        # Fetch data by week   
        elif post_data['duration'] == 2:
            response_object = load_mock_data('mock_data/key_metrix.json')
            response_object['status'] = 'success'
        # Fetch data by month
        elif post_data['duration'] == 3:
            response_object = load_mock_data('mock_data/key_metrix.json')
            response_object['status'] = 'success'
        # Fetch data overall
        elif post_data['duration'] == 4:
            response_object = load_mock_data('mock_data/key_metrix.json')
            response_object['status'] = 'success'
        # Wrong input
        else:
            response_object['status'] = 'fail'
            response_object['message'] = 'Wrong input duration!'
    return response_object

# Change ngrok_url, and Run
@app.route("/setwebhook")
def setwebhook():
    ngrok_url = NGROK
    url = "https://"+ngrok_url+".ngrok.io/"
    s = requests.get("https://api.telegram.org/bot{}/setWebhook?url={}".format(TOKEN,url))
    if s:
        return "yes"
    else:
        return "fail"

@app.route("/survey")
def survey():
    return "survey"

@app.route('/create', methods=('GET', 'POST'))
def create():
    if request.method == 'POST':
        type = request.form['type']
        print(type)
        if type == 'text':
            create_url=url_for('createText')
        else:
            create_url=url_for('createSurvey')
        return redirect(create_url)

    return render_template('create.html')

@app.route('/create/text', methods=('GET', 'POST'))
def createText():
    if request.method == 'POST':
        msg_dict['type'] = "text"
        msg_dict['content'] = request.form['text']
        set_time_url = url_for('setPushTime')
        return redirect(set_time_url)

    return render_template('create_text.html')

@app.route('/create/survey', methods=('GET', 'POST'))
def createSurvey():
    if request.method == 'POST':
        msg_dict['type'] = "survey"
        msg_dict['question'] = request.form['question']
        msg_dict['option'] = request.form['option']
        msg_dict['content'] = request.form['content']
        set_time_url = url_for('setPushTime')
        return redirect(set_time_url)

    return render_template('create_survey.html')

@app.route('/set_period', methods=('GET', 'POST'))
def setPushTime():
    if request.method == 'POST':
        time = request.form['time']
        date = request.form['date']
        period = request.form['period']
        target = request.form['target']

        with sql.connect("database.db") as con:
            cur = con.cursor()
            cur.execute("INSERT INTO " + TABLE_NAME + " (type, question, option, content, date, time, period, target) VALUES (?,?,?,?,?,?,?,?)",(msg_dict['type'], msg_dict['question'], msg_dict['option'], msg_dict['content'], date, time, period, target) )
            con.commit()
            print("update successfully")
            cur.close()
            push_message()
        feedback_url = url_for('setFeedback')
        return redirect(feedback_url)
    return render_template('set_time.html')

@app.route('/feedback', methods=('GET', 'POST'))
def setFeedback():
    if request.method == 'POST':
        try:
            os.system("python3 src\statistic\scenario2\scenario2_statistics.py")
        except:
            print("Failed to finish running scenario2_statistics.py")
        return redirect("http://localhost:9527/#/dialog/index")
    return render_template('feedback.html')

if __name__ == "__main__":
    corpus = CORPUS
    createQuestionDB(corpus)
    app.run(debug=True)
