#!/usr/bin/env python
# coding: utf-8

# import the corpus 

import json

with open('corpus/corpus.json', encoding='utf-8') as json_file:
	data = json.load(json_file)

#print (type(data))


# get all tags in the corpus

all_tags = []

for item in data:
    tag = item['tag']
    all_tags.append(tag)

#print (all_tags)

# Use string matching algorithm to find the best matching tags.
# Fuzzywuzzy: a string matching module. It uses Levenshtein Distance to calculate the differences between sequences. It can Find the most similar sentences from a large set of candidates.

from fuzzywuzzy import fuzz,process

def match_input_with_tags(input): # input: string
    
    matching_results = process.extract(input, all_tags, limit=3)
    
    top_tags = []
    for item in matching_results:
        top_tag = item[0]
        top_tag = "/" + top_tag
        top_tags.append(top_tag)
    
    # change format
    str_top_tags = []
    for i in range(len(top_tags)):
        a = top_tags[i]
        b = str(a)
        str_top_tags.append(b)
    #print (str_top_tags)
    
    # print response
    res = "Here are labels you may want to choose:\n"
    for i in str_top_tags:
        res = res + i + '\n'
    res += "Click a label to see corresponding questions."
    print(res)
    return res

# get the related answer to the tag selected by the user

def return_answer_to_selected_tag(selected_tag): # selected_tag: string
    
    selected_index = all_tags.index(selected_tag)
    selected_answer = data[selected_index]['answer']
    
    return selected_answer


# user_input = 'Application'

# # best matching tags with the user input
# match_input_with_tags(input=user_input) 


# user_select = 'Application Fee'

# # the related answer to the tag selected by the user
# return_answer_to_selected_tag(selected_tag=user_select)

